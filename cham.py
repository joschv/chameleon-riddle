import random


def brute_force1(red, green, blue):
    global COUNTER_BRUTE1, COUNTER_B1_LEN
    COUNTER_BRUTE1 = 0
    COUNTER_B1_LEN = 0
    print("BRUTE FORCE")
    print("Initial state: Red " + str(red) + ", Green " + str(green) + ", Blue " + str(blue))
    if brute1_transition(0, red, green, blue):
        print("Solved!")
    else:
        print("Not Solved!")
    print("Recursive steps: " + str(COUNTER_BRUTE1))


def brute1_transition(n_steps, red, green, blue):
    global COUNTER_BRUTE1, COUNTER_B1_SOLV, COUNTER_B1_LEN
    COUNTER_BRUTE1 += 1
    if is_final_state(red, green, blue):
        print("Final state found: Red " + str(red) + ", Green " + str(green)
              + ", Blue " + str(blue) + " after " + str(n_steps) + " steps!")
        COUNTER_B1_SOLV += 1
        COUNTER_B1_LEN = n_steps
        return True
    if red >= 0 and green >= 0 and blue >= 0 and n_steps < STEP_LIMIT:
        if brute1_transition(n_steps + 1, red + 2, green - 1, blue - 1):
            return True
        if brute1_transition(n_steps + 1, red - 1, green + 2, blue - 1):
            return True
        if brute1_transition(n_steps + 1, red - 1, green - 1, blue + 2):
            return True
    return False


def brute_force2(red, green, blue):
    global COUNTER_BRUTE2, COUNTER_B2_LEN
    COUNTER_BRUTE2 = 0
    COUNTER_B2_LEN = 0
    print("SMART BRUTE FORCE")
    print("Initial state: Red " + str(red) + ", Green " + str(green) + ", Blue " + str(blue))
    if brute2_transition(0, red, green, blue):
        print("Solved!")
    else:
        print("Not Solved!")
    print("Recursive steps: " + str(COUNTER_BRUTE2))


def brute2_transition(n_steps, red, green, blue):
    global COUNTER_BRUTE2, COUNTER_B2_SOLV, COUNTER_B2_LEN
    COUNTER_BRUTE2 += 1
    if is_final_state(red, green, blue):
        print("Final state found: Red " + str(red) + ", Green " + str(green)
              + ", Blue " + str(blue) + " after " + str(n_steps) + " steps!")
        COUNTER_B2_SOLV += 1
        COUNTER_B2_LEN = n_steps
        return True
    if is_equal_state(red, green, blue):
        steps = make_final(n_steps, red, green, blue)
        COUNTER_BRUTE2 += steps
        n_steps += steps
        COUNTER_B2_LEN = n_steps
        COUNTER_B2_SOLV += 1
        return True
    if red >= 0 and green >= 0 and blue >= 0 and n_steps < STEP_LIMIT:
        if brute2_transition(n_steps + 1, red + 2, green - 1, blue - 1):
            return True
        if brute2_transition(n_steps + 1, red - 1, green + 2, blue - 1):
            return True
        if brute2_transition(n_steps + 1, red - 1, green - 1, blue + 2):
            return True
    return False


def search_equal(red, green, blue):
    # init
    global COUNTER_SEARCH, COUNTER_SE_SOLV, COUNTER_SE_LEN
    COUNTER_SEARCH = 0
    COUNTER_SE_LEN = 0
    red_last = green_last = blue_last = 0
    cycle = False
    n_steps = 0
    print("SEARCH EQUAL STATE")
    print("Initial state: Red " + str(red) + ", Green " + str(green) + ", Blue " + str(blue))
    # try to equalize
    while not is_equal_state(red, green, blue) and not cycle:
        if min(red, green, blue) == red:
            red += 2
            green -= 1
            blue -= 1
            n_steps += 1
            COUNTER_SEARCH += 1
            red_last = 3
        elif min(red, green, blue) == green:
            red -= 1
            green += 2
            blue -= 1
            n_steps += 1
            COUNTER_SEARCH += 1
            green_last = 3
        elif min(red, green, blue) == blue:
            red -= 1
            green -= 1
            blue += 2
            n_steps += 1
            COUNTER_SEARCH += 1
            blue_last = 3
        if red_last > 0 and green_last > 0 and blue_last > 0:
            cycle = True
        red_last -= 1
        green_last -= 1
        blue_last -= 1
    if cycle:
        print("Not solvable!")
        print("Last state: Red " + str(red) + ", Green " + str(green)
              + ", Blue " + str(blue) + " after " + str(n_steps) + " steps!")
    else:
        print("Solved!")
        COUNTER_SE_SOLV += 1
    if is_equal_state(red, green, blue):
        steps = make_final(n_steps, red, green, blue)
        n_steps += steps
        COUNTER_SEARCH += steps
    if is_final_state(red, green, blue):
        print("Final state found: Red " + str(red) + ", Green " + str(green)
              + ", Blue " + str(blue) + " after " + str(n_steps) + " steps!")
    COUNTER_SE_LEN = n_steps


def is_equal_state(red, green, blue):
    return red == green or green == blue or red == blue


def make_final(n_steps, red, green, blue):
    steps = 0
    final = False
    if red == green:
        blue += red + green
        steps += red
        red = green = 0
        final = True
    if green == blue:
        red += green + blue
        steps += green
        green = blue = 0
        final = True
    if blue == red:
        green += blue + red
        steps += blue
        blue = red = 0
        final = True
    if final:
        print("Final state found: Red " + str(red) + ", Green " + str(green)
              + ", Blue " + str(blue) + " after " + str(n_steps + steps) + " steps!")
    return steps


def is_final_state(red, green, blue):
    if (red == 0 and green == 0) or (green == 0 and blue == 0) or (blue == 0 and red == 0):
        return True
    return False


def math_algo(red, green, blue):
    global COUNTER_MATH, COUNTER_MA_LEN
    COUNTER_MATH = 0
    COUNTER_MA_LEN = 0
    print("MATH ALGORITHM")
    print("Initial state: Red " + str(red) + ", Green " + str(green) + ", Blue " + str(blue))
    if not solvable(red, green, blue):
        print("Not solvable! Zero steps taken.")
    else:
        math_search(red, green, blue)


def math_search(red, green, blue):
    global COUNTER_MA_SOLV, COUNTER_MATH, COUNTER_MA_LEN
    n_steps = 0
    if red % 3 == green % 3:
        while not is_equal_state(red, green, blue):
            # keep 3rd color alive
            if blue < 1:
                red -= 1
                green -= 1
                blue += 2
            else:
                # make equal
                diff = red - green
                steps = diff / 3
                if steps > 0:
                    red -= 1
                    green += 2
                elif steps < 0:
                    red += 2
                    green -= 1
                blue -= 1
            n_steps += 1
    elif green % 3 == blue % 3:
        while not is_equal_state(red, green, blue):
            # keep 3rd color alive
            if red < 1:
                red += 2
                green -= 1
                blue -= 1
            else:
                # make equal
                diff = green - blue
                steps = diff / 3
                if steps > 0:
                    green -= 1
                    blue += 2
                elif steps < 0:
                    green += 2
                    blue -= 1
                red -= 1
            n_steps += 1
    elif blue % 3 == red % 3:
        while not is_equal_state(red, green, blue):
            # keep 3rd color alive
            if green < 1:
                red -= 1
                green += 2
                blue -= 1
            else:
                # make equal
                diff = blue - red
                steps = diff / 3
                if steps > 0:
                    red += 2
                    blue -= 1
                elif steps < 0:
                    red -= 1
                    blue += 2
                green -= 1
            n_steps += 1
    # make equal state final
    n_steps += make_final(n_steps, red, green, blue)
    COUNTER_MATH = n_steps
    COUNTER_MA_LEN = n_steps
    COUNTER_MA_SOLV += 1


def solvable(red, green, blue):
    return red % 3 == green % 3 or red % 3 == blue % 3 or green % 3 == blue % 3


def run_test(n, max_c):
    # init
    cnt_bf1 = cnt_bf2 = cnt_search = cnt_math = 0
    cnt_sl_bf1 = cnt_sl_bf2 = cnt_sl_se = cnt_sl_ma = 0
    n_solvable_cases = 0
    # test loops
    for i in range(1, n):
        # generate initial state
        red = random.randint(1, max_c)
        green = random.randint(1, max_c)
        blue = random.randint(1, max_c)
        # check solvability
        if solvable(red, green, blue):
            print("Solvable")
            n_solvable_cases += 1
        else:
            print("Not solvable")
        # and run algorithms
        brute_force1(red, green, blue)
        brute_force2(red, green, blue)
        search_equal(red, green, blue)
        math_algo(red, green, blue)
        # count stats
        cnt_bf1 += COUNTER_BRUTE1
        cnt_bf2 += COUNTER_BRUTE2
        cnt_search += COUNTER_SEARCH
        cnt_math += COUNTER_MATH

        cnt_sl_bf1 += COUNTER_B1_LEN
        cnt_sl_bf2 += COUNTER_B2_LEN
        cnt_sl_se += COUNTER_SE_LEN
        cnt_sl_ma += COUNTER_MA_LEN
    # calculate average transitions
    cnt_bf1 /= n_tests
    cnt_bf2 /= n_tests
    cnt_search /= n_tests
    cnt_math /= n_tests
    # calculate average solution length
    if COUNTER_B1_SOLV == 0:
        cnt_sl_bf1 = "No solutions"
    else:
        cnt_sl_bf1 /= COUNTER_B1_SOLV
    if COUNTER_B2_SOLV == 0:
        cnt_sl_bf2 = "No solutions"
    else:
        cnt_sl_bf2 /= COUNTER_B2_SOLV
    if COUNTER_SE_SOLV == 0:
        cnt_sl_se = "No solutions"
    else:
        cnt_sl_se /= COUNTER_SE_SOLV
    if COUNTER_MA_SOLV == 0:
        cnt_sl_ma = "No solutions"
    else:
        cnt_sl_ma /= COUNTER_MA_SOLV
    # print results
    print("\nRESULT\n" + str(n) + " tests with random initial states with random numbers of chameleons up to\n"
          + str(max_c) + " of each color and a step limit for brute forcing of " + str(STEP_LIMIT) + ".")

    print("\n\t\t\t\t\t\t\t\t\t\tAverage transition steps")
    print("Brute force \t\t\t\t\t\t\t" + str(cnt_bf1))
    print("Brute force with equal-state detection \t" + str(cnt_bf2))
    print("Search-equal \t\t\t\t\t\t\t" + str(cnt_search))
    print("Math algorithm \t\t\t\t\t\t\t" + str(cnt_math))

    print("\n\t\t\t\t\t\t\t\t\t\tAverage solution length")
    print("Brute force \t\t\t\t\t\t\t" + str(cnt_sl_bf1))
    print("Brute force with equal-state detection \t" + str(cnt_sl_bf2))
    print("Search-equal \t\t\t\t\t\t\t" + str(cnt_sl_se))
    print("Math algorithm \t\t\t\t\t\t\t" + str(cnt_sl_ma))

    print("\n\t\t\t\t\t\t\t\t\t\tNo. of solved riddles")
    print("Brute force \t\t\t\t\t\t\t" + str(COUNTER_B1_SOLV))
    print("Brute force with equal-state detection \t" + str(COUNTER_B2_SOLV))
    print("Search-equal \t\t\t\t\t\t\t" + str(COUNTER_SE_SOLV))
    print("Math algorithm \t\t\t\t\t\t\t" + str(COUNTER_MA_SOLV))
    print("Solvable riddles\t\t\t\t\t\t" + str(n_solvable_cases))

    print("\n\t\t\t\t\t\t\t\t\t\tStep ratio")
    print("BFwEqStDet:BF \t\t\t\t\t\t\t1:" + str(cnt_bf1/cnt_bf2))
    print("SearchEq:BF \t\t\t\t\t\t\t1:" + str(cnt_bf1/cnt_search))
    print("Math:SearchEq \t\t\t\t\t\t\t1:" + str(cnt_search / cnt_math))


# step limit
STEP_LIMIT = 12

# transitions counter
COUNTER_BRUTE1 = 0
COUNTER_BRUTE2 = 0
COUNTER_SEARCH = 0
COUNTER_MATH = 0

# solution length counter
COUNTER_B1_LEN = 0
COUNTER_B2_LEN = 0
COUNTER_SE_LEN = 0
COUNTER_MA_LEN = 0

# solve counter
COUNTER_B1_SOLV = 0
COUNTER_B2_SOLV = 0
COUNTER_SE_SOLV = 0
COUNTER_MA_SOLV = 0


if __name__ == '__main__':
    n_tests = 100
    max_chameleons = 6
    run_test(n_tests, max_chameleons)
    # math_algo(16, 1, 24)
